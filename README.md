# Point cloud based classification of 3D Models

To run on [ModelNet10](http://modelnet.cs.princeton.edu), extract the ModelNet10 data such that a folder 'ModelNet10' containing the categories right inside it is present in the same folder as 'classifier.py'

For [ESB](https://engineering.purdue.edu/PRECISE/shrec08) (CAD) dataset, place the folders corresponding to the classes described below inside, a folder 'data' which must be in the same location as 'classifier.py'

Tensorboard can be readily used. 
Just issue 'tensorboard --logdir=model_dir' in the same folder.

At the time the models were trained, the Tensorflow version used was 1.4

The code is same for both the ModelNet and CAD data, except some variables like filename for training data.

### Dependencies:

Python 3

[Trimesh](https://github.com/mikedh/trimesh) library for Python

Tensorflow (should install remaining requirements)

## ModelNet10
Classification on ModelNet10 dataset. 

Accuracy: 86.34%

![Result 1](figs/Figure_1.png?raw=true "ModelNet10 Result")

## Princeton ESB
A subset of ESB dataset was used as some classes in it have very small number of samples. 

The classes 'Discs', '90 degree elbows', 'Bolt Like Parts', 'Gear like Parts', 'Cylindrical Parts', 'Long Pins' from the group 'Solids of Revolution' were used.

Overall accuracy: 98.14%


![Result 2](figs/Figure_2.png?raw=true "ESB Result")
