import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import trimesh

EXTENT = 100.0
CLEARANCE = 0.1
L2PARAM = 0.02
LEARNING_RATE = 5e-5
B1 = 0.9
B2 = 0.9

BATCH_TRAIN = 8
BATCH_EVAL = 9

eval_start_delay = 150
eval_delay = 150

REG_PARAM = 0.05

def triangle_area(v1,v2,v3):
    return 0.5 * np.linalg.norm(np.cross(v2-v1, v3-v1),axis = 1)

def createGrid(mesh, random_num_pts = False, n = 2**11, use_seed = False):
    if use_seed:
        np.random.seed(8)
    faces = mesh.faces
    v1s,v2s,v3s = mesh.vertices[faces[:,0],:],mesh.vertices[faces[:,1],:],mesh.vertices[faces[:,2],:]

    areas = triangle_area(v1s,v2s,v3s)

    probs = areas/areas.sum()
    # number of points to sample
    if random_num_pts == True:
        n = np.random.randint(2**10,2**14)
    weighted_rand_inds = np.random.choice(range(len(areas)),size = n, p = probs)

    sel_v1s = v1s[weighted_rand_inds]
    sel_v2s = v2s[weighted_rand_inds]
    sel_v3s = v3s[weighted_rand_inds]

    # barycentric co-ords
    u = np.random.rand(n,1)
    v = np.random.rand(n,1)

    invalids = u + v >1 # u+v+w =1

    u[invalids] = 1 - u[invalids]
    v[invalids] = 1 - v[invalids]

    w = 1-(u+v)

    pt_cld = (sel_v1s * u) + (sel_v2s * v) + (sel_v3s * w)

    # centering the model
    delta = (np.max(pt_cld,axis=0) + np.min(pt_cld,axis=0))/2.0
    if sum(np.abs(delta)) != 0.0:
        pt_cld -= delta

    # scaling the model to have largest extent of 100
    span = np.max(np.max(pt_cld,axis=0) - np.min(pt_cld,axis=0))
    pt_cld /= span
    pt_cld *= (EXTENT - 2*CLEARANCE)
    pt_cld += EXTENT/2.0    # to get indices correctly on dividing by unit length


    matrices = [np.zeros((128,128,128)),
    np.zeros((64,64,64)),
    np.zeros((32,32,32)),
    np.zeros((16,16,16)),
    np.zeros((8,8,8)),
    np.zeros((4,4,4))]

    for D in range(len(matrices)):
        unit = EXTENT/float(2**(len(matrices) - D + 1))
        inds = (pt_cld/unit).astype(int)
        for i in inds:
            matrices[D][tuple(i)] = 1
        matrices[D] /= np.mean(matrices[D])

    return np.array(matrices)

TRAIN_FILE_LIST = 'train_files.txt'
EVAL_FILE_LIST = 'val_files.txt'
NUM_CLASSES = 6

with open(EVAL_FILE_LIST,'r') as f:
        evalFiles = np.array(f.read().split('\n')[:-1])
NUM_FILES_EVAL = evalFiles.shape[0]

CAT_DICT = {'Discs':0, \
'90 degree elbows':1, \
'Bolt Like Parts':2, \
'Gear like Parts':3, \
'Cylindrical Parts':4, \
'Long Pins':5 }

CATEGORIES = ['Discs', '90 degree elbows', 'Bolt Like Parts', 'Gear like Parts', 'Cylindrical Parts', 'Long Pins']

tf.logging.set_verbosity(tf.logging.INFO)

def Convo3d(layer,num_filters, isTrain):
    cnv =  tf.layers.conv3d(layer,num_filters,[5,5,5],padding = 'same',\
    activation = tf.nn.relu, kernel_initializer = tf.contrib.layers.xavier_initializer())
    return cnv

def Mpool(layer):
    return tf.layers.max_pooling3d(layer,[2,2,2],2)

def model_fn(features,labels,mode):
    isTrain = (mode == tf.estimator.ModeKeys.TRAIN)
    input64 = tf.reshape(features['64'], [-1, 64, 64, 64, 1])

    c2a = Convo3d(input64,16,isTrain)
    c2b = Convo3d(c2a,16,isTrain)
    p2 = Mpool(c2b) # [-1,32,32,32,f]

    c3a = Convo3d(p2,16,isTrain)
    c3b = Convo3d(c3a,16,isTrain)
    p3 = Mpool(c3b) # [-1,16,16,16,f]

    c4a = Convo3d(p3,16,isTrain)
    c4b = Convo3d(c4a,16,isTrain)
    p4 = Mpool(c4b) # [-1,8,8,8,f]

    c5a = Convo3d(p4,16,isTrain)
    c5b = Convo3d(c5a,16,isTrain)
    dense1 = tf.layers.dense(tf.layers.Flatten()(c5b),128,activation=tf.nn.relu)
    logits = tf.layers.dense(dense1,NUM_CLASSES)

    if mode != tf.estimator.ModeKeys.PREDICT:
        loss = tf.losses.sparse_softmax_cross_entropy(labels,logits)

    if mode == tf.estimator.ModeKeys.TRAIN:
        optimizer = tf.train.AdamOptimizer(learning_rate=LEARNING_RATE, beta1=B1, beta2=B2)
        train_op = optimizer.minimize(loss,global_step=tf.train.get_global_step())
        return tf.estimator.EstimatorSpec(mode=mode, loss=loss, train_op=train_op)
    else:
        pred = {"predictions": tf.argmax(input = logits,axis = 1)}
        if mode == tf.estimator.ModeKeys.EVAL:
            eval_metric_ops = {
            "accuracy": tf.metrics.accuracy(labels=labels, predictions=pred["predictions"])}
            return tf.estimator.EstimatorSpec(mode=mode, loss=loss, eval_metric_ops=eval_metric_ops)
        if mode == tf.estimator.ModeKeys.PREDICT:
            return tf.estimator.EstimatorSpec(mode=mode, predictions=pred["predictions"])

class IteratorInitializerHook(tf.train.SessionRunHook):
    def __init__(self):
        super(IteratorInitializerHook, self).__init__()
        self.iterator_initializer_func = None

    def after_create_session(self, session, coord):
        self.iterator_initializer_func(session)

def train_sample_gen():
    with open(TRAIN_FILE_LIST,'r') as f:
        trainFiles = np.array(f.read().split('\n')[:-1])
    num_files = trainFiles.shape[0]
    trainFiles = trainFiles[np.random.permutation(num_files)]
    trainFiles = trainFiles[np.random.permutation(num_files)]
    trainFiles = trainFiles[np.random.permutation(num_files)]

    count = -1
    while(1):
        count += 1
        if count >= num_files:
            count = 0
            trainFiles = trainFiles[np.random.permutation(num_files)]

        # preparing the lable file
        cat = trainFiles[count].split('/')[1]
        labels = CAT_DICT[cat]

        # Input file
        mesh = trimesh.load_mesh(trainFiles[count])

        inputFull = createGrid(mesh)

        yield inputFull[0].astype(np.float32), \
        inputFull[1].astype(np.float32), \
        inputFull[2].astype(np.float32), \
        inputFull[3].astype(np.float32), \
        inputFull[4].astype(np.float32), \
        inputFull[5].astype(np.float32), \
        labels

def get_train_inputs(batch_size):
    iterator_initializer_hook = IteratorInitializerHook()

    def train_inputs():
        with tf.name_scope('Training_data'):
            dataset = tf.data.Dataset.from_generator(
            generator = train_sample_gen,
            output_types = (tf.float32, tf.float32, tf.float32, tf.float32, tf.float32, tf.float32, tf.int64),
            output_shapes= ([128,128,128], [64,64,64], [32,32,32], [16,16,16], [8,8,8], [4,4,4], []))

            dataset = dataset.repeat(None)
            dataset = dataset.batch(batch_size)

            iterator = dataset.make_initializable_iterator()
            inp128, inp64, inp32, inp16, inp8, inp4, label = iterator.get_next()
            iterator_initializer_hook.iterator_initializer_func = \
                lambda sess: sess.run(
                    iterator.initializer)
            return {'128':inp128, '64':inp64, '32':inp32, '16':inp16, '8':inp8, '4':inp4}, label
    return train_inputs, iterator_initializer_hook

def eval_sample_gen():
    with open(EVAL_FILE_LIST,'r') as f:
        evalFiles = np.array(f.read().split('\n')[:-1])
    num_files = evalFiles.shape[0]

    count = -1
    while(1):
        count += 1
        # print(count)
        if count >= num_files:
            # print('Epoch of evaluation samples!')
            count = 0
            evalFiles = evalFiles[np.random.permutation(num_files)]

        # preparing the lable file
        cat = evalFiles[count].split('/')[1]
        labels = CAT_DICT[cat]

        # Input file
        # inputFull = np.load(evalFiles[count][:-3] + 'npy', encoding='latin1')
        mesh = trimesh.load_mesh(evalFiles[count])
        inputFull = createGrid(mesh, use_seed=True)

        yield inputFull[0].astype(np.float32), \
        inputFull[1].astype(np.float32), \
        inputFull[2].astype(np.float32), \
        inputFull[3].astype(np.float32), \
        inputFull[4].astype(np.float32), \
        inputFull[5].astype(np.float32), \
        labels

def get_eval_inputs(batch_size):
    iterator_initializer_hook = IteratorInitializerHook()

    def eval_inputs():
        with tf.name_scope('Evaluation_data'):
            dataset = tf.data.Dataset.from_generator(
            generator = eval_sample_gen,
            output_types = (tf.float32, tf.float32, tf.float32, tf.float32, tf.float32, tf.float32, tf.int64),
            output_shapes= ([128,128,128], [64,64,64], [32,32,32], [16,16,16], [8,8,8], [4,4,4], []))

            dataset = dataset.repeat(None)
            dataset = dataset.batch(batch_size)

            iterator = dataset.make_initializable_iterator()
            inp128, inp64, inp32, inp16, inp8, inp4, label = iterator.get_next()
            iterator_initializer_hook.iterator_initializer_func = \
                lambda sess: sess.run(
                    iterator.initializer)
            return {'128':inp128, '64':inp64, '32':inp32, '16':inp16, '8':inp8, '4':inp4}, label
    return eval_inputs, iterator_initializer_hook

def main(unused_argv):
    mycfg = tf.estimator.RunConfig(model_dir=None,
    tf_random_seed=None,
    save_summary_steps=100,
    save_checkpoints_steps=100,
    save_checkpoints_secs=None,
    session_config=None,
    keep_checkpoint_max=800,
    keep_checkpoint_every_n_hours=10000,
    log_step_count_steps=100)

    est = tf.estimator.Estimator(model_fn=model_fn, model_dir="./model_dir", config = mycfg)

    train_input_fn, train_input_hook = get_train_inputs(batch_size = BATCH_TRAIN)
    eval_input_fn, eval_input_hook = get_eval_inputs(batch_size = BATCH_EVAL)

    train_spec = tf.estimator.TrainSpec(
    input_fn = train_input_fn,
    hooks=[train_input_hook],
    max_steps=20000)

    eval_spec = tf.estimator.EvalSpec(
    input_fn = eval_input_fn,
    steps = int(NUM_FILES_EVAL/BATCH_EVAL),
    hooks=[eval_input_hook],
    throttle_secs=eval_delay,
    start_delay_secs=eval_start_delay
    )

    # tf.estimator.train_and_evaluate(est, train_spec, eval_spec)


    res = est.predict(input_fn = eval_input_fn, hooks=[eval_input_hook])

    labels = np.zeros(NUM_FILES_EVAL,dtype=int)
    with open(EVAL_FILE_LIST,'r') as f:
        evalFiles = np.array(f.read().split('\n')[:-1])    
    count = 0
    print('Preparing labels')
    while(count < NUM_FILES_EVAL):       
        cat = evalFiles[count].split('/')[1]
        labels[count] = int(CAT_DICT[cat])
        count += 1
    print('Done labels')

    confusion_matrix = np.zeros((NUM_CLASSES,NUM_CLASSES))
    count = 0
    for r in res:
        pred = int(r)
        print('Sample',count,'prediction : ',pred,'GT : ',labels[count])

        confusion_matrix[labels[count],pred] += 1

        count += 1
        if count >= NUM_FILES_EVAL:
            break

    tp = 0
    for i in range(NUM_CLASSES):
        tp += confusion_matrix[i,i]
    print('Accuracy',tp/NUM_FILES_EVAL)


    plt.imshow(confusion_matrix, cmap='gray')
    plt.colorbar()
    plt.show()

if __name__ == '__main__':
    tf.app.run()
